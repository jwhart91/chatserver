# README #

### What is this repository for? ###

* Users CherryPy to create a chat server, authentication handled without password exchange by public key encryption.
* v0.1

### How do I get set up? ###

* edit DataSource.py with your database information
* have CherryPi, python RSA, and the relevant database libraries installed
* run server.py with Python 3

### Web interface ###

* Web administration interface and API listen on port 19999 by default
* Exposes user data and allows to view data
* Makes use of:
  -  Javascript SHA at http://caligatio.github.com/jsSHA/
  -  JSEncrypt v2.3.1 at https://github.com/travist/jsencrypt
  -  AES-JS at https://github.com/ricmoo/aes-js
  -  JQuery at http://jquery.com/download/

### Status ###
Incomplete

### Who do I talk to? ###
jwh dot pseud circled-a gmail period com