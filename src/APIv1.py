import cherrypy, os, os.path, json, base64
from DataSource import DataSource, UserInfo, generateId, getTimestamp
from Security import Crypto

class APIv1():
    def __init__(self, jsonstruct):
        self.data = jsonstruct
        self.fmap = {"signup"                 : self.signUp,
                     "checkCaptcha"           : self.checkCaptcha,
                     "requestKeys"            : self.requestKeys,
                     "getChallengeKey"        : self.getChallengeKey,
                     "answerChallengeKey"     : self.answerChallengeKey,
                     "logoutOfSession"        : self.logoutOfSession,
                     "logoutOfAllSessions"    : self.logoutOfAllSessions,
                     "getAllLogins"           : self.getAllLogins,
                     "getUuid"                : self.getUuid,
                     "sendMessage"            : self.sendMessage,
                     "getMessages"            : self.getMessages,
                     "checkFriendRequests"    : self.checkFriendRequests,
                     "sendFriendRequest"      : self.sendFriendRequest,
                     "confirmFriend"          : self.confirmFriendRequest,
                     "denyFriend"             : self.denyFriendRequest,
                     "getFriendsList"         : self.getFriendsList,
                     "changeDisplayName"      : self.changeDisplayName,
                     "changeProfileImage"     : self.changeProfileImage,
                     "updateLocation"         : self.updateLocation,
                     "getOwnLocation"         : self.getOwnLocation,
                     "getUserInformation"     : self.getUserInformation,
                     "answerFriendRequest"    : self.answerFriendRequest,
                     "getDistanceFromUser"    : self.getDistanceFromUser,
                     "findRandomUserByArea"   : self.findRandomUserByArea,
                     "getUserCountWithinArea" : self.getUserCountWithinArea,
                     "getProfileImage"        : self.getProfileImage,
                     "setStatusMessage"       : self.setStatusMessage,
                     "setOnlineState"         : self.setOnlineState,
                     "getChatHistory"         : self.getChatHistory,
                    }



    def evaluate(self):
        result = []
        for req in self.data:
            request = req["request"]
            if request in self.fmap:
                result = result + (self.fmap[request])(req)
            else:
                error = [{"status" : "error",
                          "request": request,
                          "reason" : "Unsupported request."}]
                result = result + error
        return result



    def signUp(self, req):
        email       = req["email"]
        displayName = req["displayName"]
        pubKey      = req["publicKey"]
        encPrivKey  = req["encPrivateKey"]

        if not DataSource.doesEmailExist(cherrypy.request.db, email):
            capId = DataSource.makeCaptcha(cherrypy.request.db, email)
            return [{"status"    : "success",
                     "captchaId" : capId,
                     "response"  : "captchaChallenge"}]
        else:
            return [{"status" : "error",
                     "reason" : "emailExists"}]



    def checkCaptcha(self, req):
        database = cherrypy.request.db
        capVal      = req["capVal"]
        email       = req["email"]
        capId       = req["captchaID"]
        displayName = req["displayName"]
        pubKey      = req["publicKey"]
        encPrivKey  = req["encPrivateKey"]

        print("Checking captcha...." + capId + " with val: " + capVal)
        if not DataSource.checkCaptcha(database, capId, email, capVal):
            capId = DataSource.makeCaptcha(database, email)
            result = [{"status"    : "error",
                       "response"  : "badCaptcha",
                       "captchaId" : capId}]
            return result
        else:
            if DataSource.isUserActivated(database, email):
                uuid = DataSource.getUserID(database, email)
                result = [{"status" : "error",
                           "userId" : uuid,
                           "reason" : "accountAlreadyActivated"}]
            else:
                uuid = DataSource.addUser(database, email, displayName, pubKey, encPrivKey)
                result = [{"status"       : "success",
                           "userId"       : uuid,
                           "response"     : "waitingForActivation"}]
            return result

    def requestKeys(self, req):
        database = cherrypy.request.db
        if "uuid" not in req and "email" not in req:
            return [{"status", "error",
                     "reason", "noInformationSupplied"}]

        if "email" in req:
            uuid = DataSource.getUserID(database, req["email"])
            email = req["email"]
        elif DataSource.doesUuidExist(database, req["uuid"]):
            uuid = req["uuid"]
            email = DataSource.getUserEmail(database, uuid)
        else:
            return [{"status": "error",
                     "reason": "userDoesNotExist"}]


        state = DataSource.getKeysByUuid(database, uuid)
        if state[0] == True:
            result = [{"status"    : "success",
                       "response"  : "keyDelivery",
                       "email"     : email,
                       "uuid"      : uuid,
                       "pubKey"    : state[1]["pubKey"],
                       "encPrivKey": state[1]["encPrivKey"]}]
        else:
            result = [{"status" : "error",
                       "reason" : state[1]}]
        return result

    def getChallengeKey(self, req):
        database = cherrypy.request.db
        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "noInformationSupplied"}]

        if DataSource.doesUuidExist(database, req["uuid"]):
            uuid = req["uuid"]

        if uuid == False or uuid == None:
            return [{"status": "error",
                     "reason": "userNotFound"}]

        state = DataSource.getKeysByUuid(database, uuid)
        if state[0] == True:
            challengeKey = DataSource.makeChallengeKey(database, uuid, state[1]["pubKey"])
            result = [{"status"      : "success",
                       "response"    : "issueChallengeKey",
                       "challengeKey": challengeKey}]
        else:
            result = [{"status" : "error",
                       "reason" : state[1]}]
        return result

    def answerChallengeKey(self, req):
        database = cherrypy.request.db
        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "noInformationSupplied"}]

        if "solution" not in req:
            return [{"status": "error",
                     "reason": "noSolutionProvided"}]
        sol = req["solution"]

        if DataSource.doesUuidExist(database, req["uuid"]):
            uuid = req["uuid"]
        else:
            return [{"status": "error",
                     "reason": "userNotFound"}]

        if uuid == False or uuid == None:
            return [{"status": "error",
                     "reason": "userNotFound"}]

        (state, reason) = DataSource.checkChallengeKey(database, uuid, sol)
        if state == False:
            return [{"status": "error",
                     "reason": reason}]
        else:
            loginToken = DataSource.createAuthSession(database, uuid)
            return [{"status": "success",
                     "response": "authSessionCreated",
                     "authToken": loginToken}]

    def logoutOfSession(self, req):
        database = cherrypy.request.db
        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "sessionNotSupplied"}]
        uuid = req["uuid"]

        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "sessionNotSupplied"}]
        authToken = req["authToken"]
        DataSource.invalidateSession(database, uuid, authToken)

        return [{"status": "success",
                 "response": "loggedOutOfSession",
                 "authToken": authToken}]

    def logoutOfAllSessions(self, req):
        database = cherrypy.request.db
        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSuplied"}]
        uuid = req["uuid"]
        DataSource.invalidateAllUserSession(database, uuid)

        return [{"status": "success",
                 "response": "allLoginsCleared"}]

    def getAllLogins(self, req):
        database = cherrypy.request.db
        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]
        uuid = req["uuid"]
        logins = DataSource.getAllLoginSessions(database, uuid)

        return [{"status": "success",
                 "response": "allActiveTokens",
                 "activeSessions": logins}]

    def getUuid(self, req):
        database = cherrypy.request.db
        if "email" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        uuid = DataSource.getUserID(database, req["email"])
        if uuid == False or uuid == None:
            return [{"status": "error",
                     "reason": "userNotFound"}]
        else:
            return [{"status"  : "success",
                     "response": "gotUserId",
                     "userId"  : uuid}]



    def sendMessage(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "srcUuid" not in req or "dstUuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "encMessage" not in req:
            return [{"status": "error",
                     "reason": "noMessageSupplied"}]

        srcUser = req["srcUuid"]
        destUser = req["dstUuid"]
        encMessage = req["encMessage"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, srcUser, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]
        if not DataSource.doesUuidExist(database, destUser):
            return [{"status": "error",
                     "reason": "destUserDoesNotExist"}]

        messId = DataSource.sendMessage(database, srcUser, destUser, encMessage)
        return [{"status"  : "success",
                 "response": "sentMessage",
                 "messId"  : messId}]

    def getMessages(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        uuid = req["uuid"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]
        if not DataSource.doesUuidExist(database, uuid):
            return [{"status": "error",
                     "reason": "userDoesNotExist"}]

        messages = DataSource.getMessages(database, uuid)
        return [{"status"  : "success",
                 "response": "allMessages",
                 "messages"  : messages}]


    def sendFriendRequest(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req or "prospectUuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "greeting" not in req:
            greeting = ""
        else:
            greeting = req["greeting"]

        uuid = req["uuid"]
        prospectUuid = req["prospectUuid"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]
        if not DataSource.doesUuidExist(database, prospectUuid):
            return [{"status": "error",
                     "reason": "prospectiveFriendDoesNotExist"}]

        (state, reqId) = DataSource.sendFriendRequest(database, uuid, prospectUuid, greeting)

        if state == False:
            return [{"status" : "error",
                     "reason" : reqId}]

        return [{"status"   : "success",
                 "response" : "sentFriendRequest",
                 "requestId": reqId}]

    def checkFriendRequests(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        uuid = req["uuid"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]
        if not DataSource.doesUuidExist(database, uuid):
            return [{"status": "error",
                     "reason": "userDoesNotExist"}]

        reqs = DataSource.getOutstandingFriendRequests(database, uuid)
        return [{"status"  : "success",
                 "response": "allFriendRequests",
                 "requests"  : reqs}]


    def changeDisplayName(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "newDisplayName" not in req:
            return [{"status": "error",
                     "reason": "noUpdateSupplied"}]
        authToken = req["authToken"]
        newDisplayName = req["newDisplayName"]
        uuid = req["uuid"]
        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        (state, reason) = DataSource.changeDisplayName(database, uuid, newDisplayName)
        if state == True:
            return [{"status": "error",
                     "reason": reason}]
        else:
            return [{"status"  : "success",
                     "response": "displayNameUpdated"}]

    def changeProfileImage(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "newImg" not in req:
            return [{"status": "error",
                     "reason": "noUpdateSupplied"}]

        authToken = req["authToken"]
        newProfImg = req["newImg"]
        uuid = req["uuid"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        absPath = os.path.abspath(os.getcwd())
        with open(absPath + "/files/" + uuid, "wb") as profImgFile:
            data = base64.b64decode(newProfImg)
            profImgFile.write(data)

        return [{"status"  : "success",
                 "response": "profileImageUpdated"}]



    def updateLocation(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "newLatitude" not in req or "newLongitude" not in req:
            return [{"status": "error",
                     "reason": "noUpdateSupplied"}]

        authToken = req["authToken"]
        newLat = float(req["newLatitude"])
        newLon = float(req["newLongitude"])
        uuid = req["uuid"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        DataSource.updateLocation(database, uuid, newLat, newLon)

        return [{"status"  : "success",
                 "response": "locationUpdated"}]


    def getOwnLocation(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        authToken = req["authToken"]
        uuid = req["uuid"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        (state, ldata) = DataSource.getUserLocation(database, uuid)
        if state == False:
            return [{"status": "error",
                     "reason": "noLocationForThisUser"}]

        return [{"status"  : "success",
                 "response": "yourLocation",
                 "location": ldata}]
    
    def confirmFriendRequest(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "duuid" not in req:
            return [{"status": "error",
                     "reason": "noUpdateSupplied"}]

        authToken = req["authToken"]
        uuid = req["uuid"]
        duuid = req["duuid"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]
        
        if not DataSource.doesUuidExist(database, duuid):
            return [{"status": "error",
                     "reason": "userDoesNotExist"}]        

        (state, ldata) = DataSource.confirmFriendRequest(database, uuid, duuid)
        if state == False:
            return [{"status": "error",
                     "reason": ldata}]

        return [{"status"  : "success",
                 "response": ldata}]
    
    
    def denyFriendRequest(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "duuid" not in req:
            return [{"status": "error",
                     "reason": "noUpdateSupplied"}]

        authToken = req["authToken"]
        uuid = req["uuid"]
        duuid = req["duuid"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        if not DataSource.doesUuidExist(database, duuid):
            return [{"status": "error",
                     "reason": "userDoesNotExist"}]        

        (state, ldata) = DataSource.denyFriendRequest(database, uuid, duuid)
        if state == False:
            return [{"status": "error",
                     "reason": ldata}]

        return [{"status"  : "success",
                 "response": ldata}]
    
    
    def getFriendsList(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        uuid = req["uuid"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        friends = DataSource.getFriendsList(database, uuid)
        return [{"status"  : "success",
                 "response": "allFriends",
                 "friends"  : friends}]
    
    
    def getUserInformation(self, req):
        database = cherrypy.request.db
        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        (state, data) = DataSource.getUserInformation(database, req["uuid"])
        if state == False:
            return [{"status": "error",
                     "reason": data}]
        else:
            return [{"status"        : "success",
                     "response"      : "gotUserData",
                     "userId"        : req["uuid"],
                     "displayName"   : data["displayName"],
                     "statusMessage" : data["statusMessage"],
                     "onlineState"   : data["onlineState"]}]
        

    def answerFriendRequest(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "reqId" not in req:
            return [{"status": "error",
                     "reason": "requestIdNotSupplied"}]
        
        if "answer" not in req:
            return [{"status": "error",
                     "reason": "answerNotSupplied"}]

        uuid = req["uuid"]
        authToken = req["authToken"]
        reqId = req["reqId"]
        answer = req["answer"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        
        if not DataSource.doesOpenFriendRequestExist(database, uuid, reqId):
            return [{"status" : "error",
                     "reason" : "offerNotOpen"}]
        
        (requester, requestee) = DataSource.getFriendRequestInfo(database, reqId)
        
        if requester == False or requestee == False:
            return [{"status" : "error",
                     "reason" : "couldNotSetRelationship"}]
        
        if answer == "affirm":
            (res, reason) = DataSource.confirmFriendRequest(database, reqId, requester, requestee)
            if not res:
                return [{"status" : "error",
                         "reason" : reason}]
            else:
                return [{"status"   : "success",
                         "response" : "affirmedFriendship",
                         "reason"   : reason}]
            

        elif answer == "deny":
            (res, reason) = DataSource.denyFriendRequest(database, reqId)
            if not res:
                return [{"status" : "error",
                         "reason" : reason}]
            else:
                return [{"status"   : "success",
                         "response" : "deniedFriendship",
                         "reason"   : reason}]      
            
        return [{"status" : "error",
                 "reason" : "unspecifiedError"}]   
    
    def getDistanceFromUser(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "ownUuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]
        
        if "otherUuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        ownUuid = req["ownUuid"]
        otherUuid = req["otherUuid"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, ownUuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        (res, reason) = DataSource.getDistanceBetween(database, ownUuid, otherUuid)
        
        if res == False:
            return [{"status"  : "error",
                     "reason"  : reason}]
        else:
            return [{"status"   : "success",
                     "response" : "distanceBetween",
                     "distance" : reason}]
        
        

    def findRandomUserByArea(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]
        
        if "distance" not in req:
            return [{"status": "error",
                     "reason": "distanceNotSupplied"}]

        uuid = req["uuid"]
        distance = req["distance"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        (res, reason) = DataSource.getRandomUserWithinRadius(database, uuid, distance)
        
        if res == False:
            return [{"status"  : "error",
                     "reason"  : reason}]
        else:
            return [{"status"   : "success",
                     "response" : "randomUser",
                     "uuid"     : reason}]
        
        
    def getUserCountWithinArea(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]
        
        if "distance" not in req:
            return [{"status": "error",
                     "reason": "distanceNotSupplied"}]

        uuid = req["uuid"]
        distance = req["distance"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        (res, reason) = DataSource.getUserCountWithinArea(database, uuid, distance)
        
        if res == False:
            return [{"status"  : "error",
                     "reason"  : reason}]
        else:
            return [{"status"   : "success",
                     "response" : "userCount",
                     "users"    : reason}]        
        

    def getProfileImage(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "otherUuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        uuid = req["uuid"]
        otherUuid = req["otherUuid"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        absPath = os.path.abspath(os.getcwd())
        fileName = absPath + "/files/" + otherUuid
        
        if not os.path.exists(fileName):
            fileName = fileName = absPath + "/files/" + "default_profile.png"
            
        data = ""
        with open(fileName, "rb") as profImgFile:
            data = base64.b64encode(profImgFile.read())
        
        if data == "":
            return [{"status"  : "error",
                     "reason"  : "errorReadingFile"}]
        else:
            return [{"status"    : "success",
                     "response"  : "profileImage",
                     "uuid"      : otherUuid,
                     "imageData" : data}]    
        

    def setStatusMessage(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "newStatus" not in req:
            return [{"status": "error",
                     "reason": "updateNotSupplied"}]

        uuid = req["uuid"]
        newStatus = req["newStatus"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        res = DataSource.setStatus(database, uuid, newStatus)
        if res == False:
            return [{"status": "error",
                     "reason": "cannotChangeData"}]
        else:
            return [{"status"    : "success",
                     "response"  : "statusSet",
                     "userId"    : uuid,
                     "newStatus" : newStatus}]
        
        

    def setOnlineState(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "newState" not in req:
            return [{"status": "error",
                     "reason": "updateNotSupplied"}]

        uuid = req["uuid"]
        newState = req["newState"]
        authToken = req["authToken"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        res = DataSource.setState(database, uuid, newState)
        if res == False:
            return [{"status": "error",
                     "reason": "cannotChangeData"}]
        else:
            return [{"status"   : "success",
                     "response" : "stateSet",
                     "userId"   : uuid,
                     "newState" : newState}]
               
        

    def getChatHistory(self, req):
        database = cherrypy.request.db
        if "authToken" not in req:
            return [{"status": "error",
                     "reason": "noAuthToken"}]

        if "uuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        if "otherUuid" not in req:
            return [{"status": "error",
                     "reason": "userNotSupplied"}]

        uuid = req["uuid"]
        authToken = req["authToken"]
        otherUuid = req["otherUuid"]

        if not DataSource.checkAuthSession(database, uuid, authToken):
            return [{"status": "error",
                     "reason": "notAuthorized"}]

        history = DataSource.getChatHistory(database, uuid, otherUuid)
        return [{"status"   : "success",
                 "response" : "chatHistory",
                 "size"     : len(history),
                 "history"  : history}]
        






















