import string, random, time, uuid, base64, sqlite3, mimetypes, os, os.path, cherrypy, math
from cherrypy.process import wspbus, plugins
from Security import Crypto
from sqlalchemy import Column, String, Integer, ForeignKey, create_engine, update, func, or_, and_
from sqlalchemy.orm import scoped_session, sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mysql import LONGTEXT, BOOLEAN, FLOAT

Base = declarative_base()

class LAST_MESSAGE_ID_HACK():
    lastMessageId = 1048576

uuidnode = uuid.getnode()

def generateId():
    LAST_MESSAGE_ID_HACK.lastMessageId = LAST_MESSAGE_ID_HACK.lastMessageId + 1
    return str(uuid.uuid1(uuidnode, LAST_MESSAGE_ID_HACK.lastMessageId))


def getTimestamp():
    return str(int(time.time()))


############################################################
### Database Tables
############################################################


class UserInfo(Base):
    """
    User login table, contains everything about the user.
    """
    __tablename__     = 'UserInfo'
    id                = Column(Integer, primary_key=True)
    uuid              = Column(String(128), nullable = False, unique = True)
    emailAddr         = Column(String(128), nullable = False, unique = True)
    displayName       = Column(String(128), nullable = False)
    encPrivateKey     = Column(LONGTEXT, nullable = False)
    publicKey         = Column(LONGTEXT, nullable = False)
    profileURL        = Column(LONGTEXT)
    phoneNumber       = Column(String(64))
    language          = Column(String(64))
    creationTime      = Column(String(64))
    activated         = Column(BOOLEAN)


    @staticmethod
    def createUser(email, displayName, pubKey, encPrivKey):

        return UserInfo(uuid          = generateId(),
                        emailAddr     = email,
                        displayName   = displayName,
                        encPrivateKey = encPrivKey,
                        publicKey     = pubKey,
                        profileURL    = "Null",
                        phoneNumber   = "Null",
                        language      = "EN",
                        creationTime  = getTimestamp(),
                        activated     = False)


class ActiveLogins(Base):
    """
    User login table, contains data about active logins.
    """
    __tablename__ = 'ActiveLogins'
    id            = Column(Integer, primary_key=True)
    uuid          = Column(String(128), nullable = False)
    loginToken    = Column(String(128), nullable = False, unique = True)
    creationTime  = Column(String(64))

    @staticmethod
    def createSession(uuid):
        return ActiveLogins(uuid         = uuid,
                            loginToken   = generateId(),
                            creationTime = getTimestamp())


class Messages(Base):
    """
    Message table, contains messages between users
    """
    __tablename__ = 'Messages'
    id            = Column(Integer, primary_key=True)
    srcUuid       = Column(String(128), nullable = False)
    desUuid       = Column(String(128), nullable = False)
    messageId     = Column(String(128), nullable = False)
    messageData   = Column(LONGTEXT, nullable = False)
    seen          = Column(BOOLEAN, nullable = False)
    seenTime      = Column(String(64))
    creationTime  = Column(String(64))

    @staticmethod
    def createMessage(src_uuid, dst_uuid, message):
        return Messages(srcUuid      = src_uuid,
                        desUuid      = dst_uuid,
                        messageData  = message,
                        seen         = False,
                        seenTime     = "",
                        messageId    = generateId(),
                        creationTime = getTimestamp())
    

    
class Friends(Base):
    """
    Friends table, contains friendship relationships that
    have been confirmed
    """
    __tablename__ = 'Friend'
    id            = Column(Integer, primary_key=True)
    friendA       = Column(String(128), nullable = False)
    friendB       = Column(LONGTEXT, nullable = False)
    friendshipId  = Column(String(128), nullable = False)
    creationTime  = Column(String(64))

    @staticmethod
    def createFriendship(friendA, dst_uuid):
        return Friends(friendA      = friendA,
                       friendB      = dst_uuid,
                       friendshipId = generateId(),
                       creationTime = getTimestamp())    
    
class UserStatuses(Base):
    """
    Contains the status messages and online states of users
    """
    __tablename__  = 'UserStatuses'
    id             = Column(Integer, primary_key=True)
    uuid           = Column(String(128), nullable = False, unique = True)
    status         = Column(LONGTEXT, nullable = False)
    state          = Column(String(128), nullable = False)
    alterationTime = Column(String(64))

    @staticmethod
    def createStatus(uuid, status, state):
        return UserStatuses(uuid           = uuid,
                            status         = status,
                            state          = state,
                            alterationTime = getTimestamp())        



class FriendshipRequests(Base):
    """
    FriendshipRequests table, contains friendship requests
    """
    __tablename__ = 'FriendshipRequests'
    id            = Column(Integer, primary_key=True)
    requester     = Column(String(128), nullable = False)
    requestee     = Column(String(128), nullable = False)
    requestId     = Column(String(128), nullable = False)
    presented     = Column(BOOLEAN, nullable = False)
    greeting      = Column(LONGTEXT, nullable = False)
    creationTime  = Column(String(64))

    @staticmethod
    def createFriendship(requester, requestee, greeting):
        return FriendshipRequests(requester    = requester,
                                  requestee    = requestee,
                                  presented    = False,
                                  greeting     = greeting,
                                  requestId    = generateId(),
                                  creationTime = getTimestamp())

class Locations(Base):
    """
    Locations table, contains locations of users
    """
    __tablename__ = 'Locations'
    id            = Column(Integer, primary_key=True)
    uuid          = Column(String(128), nullable = False)
    latitude      = Column(FLOAT)
    longitude     = Column(FLOAT)

    @staticmethod
    def createLocation(uuid, latitude, longitude):
        return Locations(uuid      = uuid,
                         latitude  = latitude,
                         longitude = longitude)



################################################################
################################################################
################################################################
class MemoryOnlyStorage():
    openCaptchas = {} # ID : (email, solution, timestamp)
    openChallenges = {} # UUID : (decryptedKey, encryptedKey, timestamp)

class DataSource(cherrypy.Tool):
    def __init__(self):
        cherrypy.Tool.__init__(self, 'on_start_resource', self.bind_session, priority=20)
        self.session = scoped_session(sessionmaker(autoflush=True, autocommit=False))


    def _setup(self):
        cherrypy.Tool._setup(self)
        cherrypy.request.hooks.attach('on_end_resource', self.commit_transaction, priority=80)


    def bind_session(self):
        cherrypy.engine.publish('bind', self.session)
        cherrypy.request.db = self.session


    def commit_transaction(self):
        cherrypy.request.db = None
        try:
            self.session.commit()
            self.session.flush()
        except:
            self.session.rollback()
            raise
        finally:
            self.session.remove()




################################################################
################################################################
################################################################
    @staticmethod
    def getAllLogins(db):
        """
        Get all logins
        """
        logins = db.query(UserInfo).all()
        emails = []

        for user in logins:
            emails.append(user.emailAddr)

        return emails


    @staticmethod
    def getUserInformation(db, uuid):
        user = db.query(UserInfo).filter(UserInfo.uuid == uuid).one_or_none()
        if user == None:
            return (False, "userNotFound")
        else:
            
            status = db.query(UserStatuses).filter(UserStatuses.uuid == uuid).one_or_none()
            if status == None:
                udata = {"displayName"   : user.displayName,
                         "statusMessage" : "~~ no message set ~~",
                         "onlineState"   : "Unknown"}
            else:
                udata = {"displayName"   : user.displayName,
                         "statusMessage" : status.status,
                         "onlineState"   : status.state}
        
        return (True, udata)

    @staticmethod
    def doesEmailExist(db, email):
        """
        Check the database for this email address
        """
        return len(db.query(UserInfo).filter(UserInfo.emailAddr == email).all()) != 0

    @staticmethod
    def doesUuidExist(db, uuid):
        """
        Check the database for this uuid
        """
        return len(db.query(UserInfo).filter(UserInfo.uuid == uuid).all()) != 0

    @staticmethod
    def isUserActivated(db, email):
        """
        Check if a user account is activated or not
        """
        user = db.query(UserInfo).filter(UserInfo.emailAddr == email).one_or_none()
        if user == None:
            return False
        else:
            return user.activated

    @staticmethod
    def getUserID(db, email):
        """
        Get the user ID associated with an email address
        """
        user = db.query(UserInfo).filter(UserInfo.emailAddr == email).one_or_none()
        if user == None:
            return False
        else:
            return user.uuid

    @staticmethod
    def getUserEmail(db, uuid):
        """
        Get the email address associated with an user ID
        """
        user = db.query(UserInfo).filter(UserInfo.uuid == uuid).one_or_none()
        if user == None:
            return False
        else:
            return user.emailAddr







    @staticmethod
    def deleteCaptcha(db, _id):
        """
        Remove a Captcha from the database, it will be invalid
        from this point on
        """
        if _id in MemoryOnlyStorage.openCaptchas:
            del MemoryOnlyStorage.openCaptchas[_id]

    @staticmethod
    def checkCaptcha(db, _id, email, resp):
        """
        Check if a Captcha is valid and is associated with
        the email address.
        """
        # check if the captcha even exists
        if _id not in MemoryOnlyStorage.openCaptchas:
            return (False, "captchaDoesntExist")

        capAttr = MemoryOnlyStorage.openCaptchas[_id];
        capEmail = capAttr[0];
        capSolution = capAttr[1];
        capTimestamp = int(capAttr[2]);

        # check if that captcha is associated with that email
        if email != capEmail:
            DataSource.deleteCaptcha(db, _id)
            return (False, "captchaBadUser")
        # check if the response is the same as the solution
        if capSolution != resp:
            DataSource.deleteCaptcha(db, _id)
            return (False, "captchaBadSolution")

        # Captcha expires after 5 minutes
        expires = capTimestamp + 60 * 5
        timenow = int(getTimestamp())
        if (timenow > expires):
            DataSource.deleteCaptcha(db, email, _id)
            return (False, "captchaExpired")

        # Everything checks out
        return True

    @staticmethod
    def makeCaptcha(db, email):
        """
        Make a CAPTCHA
        """
        sol = "NXaEl0"
        _id = generateId()

        MemoryOnlyStorage.openCaptchas.update({_id : (email, sol, getTimestamp())})
        return _id




    @staticmethod
    def checkActivation(db, uuid):
        """
        Check to see if the user ID is valid and that
        the user has not already been activated
        """
        user = db.query(UserInfo).filter(UserInfo.uuid == uuid).one_or_none()
        if user == None:
            return (False, "User Not Found")
        if user.activated == True:
            return (False, "Already activated")
        db.query(UserInfo).filter(UserInfo.uuid == uuid).update({UserInfo.activated : True}, synchronize_session='fetch')
        return True










    @staticmethod
    def makeChallengeKey(db, uuid, pubKey):
        """
        Make a challenge key for the user to decrypt
        """
        rawKey = generateId()
        ts = getTimestamp()
        crypter = Crypto.getInst()
        encKey = str(crypter.encryptPKCS1(rawKey, pubKey), "utf-8")
        MemoryOnlyStorage.openChallenges.update({uuid: (rawKey, encKey, ts)})
        return encKey

    @staticmethod
    def checkChallengeKey(db, uuid, rawKey):
        """
        Check if the user successfully decrypted the key, proving they
        had their password correct
        """
        if uuid not in MemoryOnlyStorage.openChallenges:
            return (False, "noChallengeFound")

        plainKey = MemoryOnlyStorage.openChallenges[uuid][0]
        del MemoryOnlyStorage.openChallenges[uuid]

        if rawKey != plainKey:
            return (False, "challengeFailed")

        return (True, "challengeAccepted")


    @staticmethod
    def addUser(db, email, dname, pubKey, encPrivKey):
        newuser = UserInfo.createUser(email, dname, pubKey, encPrivKey)
        db.add(newuser)
        return newuser.uuid

    @staticmethod
    def getKeysByEmail(db, email):
        user = db.query(UserInfo).filter(UserInfo.emailAddr == email).one_or_none()
        if user == None:
            return (False, "userNotFound")
        else:
            udata = {"pubKey"    : user.publicKey,
                     "encPrivKey": user.encPrivateKey}
            return (True, udata)

    @staticmethod
    def getKeysByUuid(db, uuid):
        user = db.query(UserInfo).filter(UserInfo.uuid == uuid).one_or_none()
        if user == None:
            return (False, "userNotFound")
        else:
            udata = {"pubKey"    : user.publicKey,
                     "encPrivKey": user.encPrivateKey}
            return (True, udata)









    @staticmethod
    def createAuthSession(db, uuid):
        newSess = ActiveLogins.createSession(uuid)
        db.add(newSess)
        return newSess.loginToken

    @staticmethod
    def invalidateSession(db, uuid, authToken):
        loginSession = db.query(ActiveLogins).filter(ActiveLogins.uuid == uuid, ActiveLogins.loginToken == authToken).one_or_none()
        if loginSession == None:
            return False
        else:
            db.delete(loginSession)
            return True

    @staticmethod
    def invalidateAllUserSession(db, uuid):
        sessions = db.query(ActiveLogins).filter(ActiveLogins.uuid == uuid).all()
        if sessions == None:
            return False
        else:
            for s in sessions:
                db.delete(s)
            return True

    @staticmethod
    def getAllLoginSessions(db, uuid):
        sessions = db.query(ActiveLogins).filter(ActiveLogins.uuid == uuid).all()
        if sessions == None:
            return False
        else:
            lst = []
            for x in sessions:
                lst = lst + [x.loginToken]

            return lst

    @staticmethod
    def checkAuthSession(db, uuid, authToken):
        loginSession = db.query(ActiveLogins).filter(ActiveLogins.uuid == uuid, ActiveLogins.loginToken == authToken).one_or_none()
        if loginSession == None:
            return False
        else:
            return True



    @staticmethod
    def sendMessage(db, src, dest, message):
        newMessage = Messages.createMessage(src, dest, message)
        db.add(newMessage)
        return newMessage.messageId


    @staticmethod
    def getMessages(db, uuid):
        messages = db.query(Messages).filter(Messages.desUuid == uuid).all()
        lst = []
        if messages == None:
            return lst
        else:
            for x in messages:
                lst = lst + [{"messageData" : x.messageData,
                              "messageId"   : x.messageId,
                              "srcUser"     : x.srcUuid,
                              "timestamp"   : x.creationTime}]
            return lst

    @staticmethod
    def sendFriendRequest(db, requester, requestee, greet):
        existing = db.query(FriendshipRequests).filter(FriendshipRequests.requester == requester, FriendshipRequests.requestee == requestee).one_or_none()
        if existing != None:
            return (False, "existingRequestOutstanding")
        newFriendship = FriendshipRequests.createFriendship(requester, requestee, greet)
        db.add(newFriendship)
        return (True, newFriendship.requestId)


    @staticmethod
    def getOutstandingFriendRequests(db, uuid):
        newRequests = db.query(FriendshipRequests).filter(FriendshipRequests.requestee == uuid).all()
        lst = []
        if newRequests == None:
            return lst
        else:
            for x in newRequests:
                lst = lst + [{"requester"   : x.requester,
                              "requestId"   : x.requestId,
                              "presented"   : x.presented,
                              "greeting"    : x.greeting,
                              "timestamp"   : x.creationTime}]
            return lst


    @staticmethod
    def changeDisplayName(db, uuid, displayName):
        user = db.query(UserInfo).filter(UserInfo.uuid == uuid).one_or_none()
        if user == None:
            return (False, "userNotFound")
        db.query(UserInfo).filter(UserInfo.uuid == uuid).update({UserInfo.displayName : displayName}, synchronize_session='fetch')
        return (True, "displayNameUpdated")


    @staticmethod
    def updateLocation(db, uuid, lat, lon):
        loc = db.query(Locations).filter(Locations.uuid == uuid).all()
        if loc == None or len(loc) < 1:
            newLoc = Locations.createLocation(uuid, lat, lon)
            db.add(newLoc)
            return True
        
        if len(loc) > 1:
            for x in loc:
                db.delete(x)
            newLoc = Locations.createLocation(uuid, lat, lon)
            db.add(newLoc)
            return True        
        db.query(Locations).filter(Locations.uuid == uuid).update({Locations.latitude : lat, Locations.longitude : lon}, synchronize_session='fetch')
        return True


    @staticmethod
    def getUserLocation(db, uuid):
        loc = db.query(Locations).filter(Locations.uuid == uuid).all()
        if loc == None or len(loc) < 1:
            return (False, "locationNotSaved")
        else:
            lData = {"latitude"  : loc[0].latitude,
                     "longitude" : loc[0].longitude}
            return (True, lData)

        
        
    @staticmethod
    def doesOpenFriendRequestExist(db, uuid, reqId):
        requests = db.query(FriendshipRequests).filter(or_(FriendshipRequests.requestee == uuid, FriendshipRequests.requester == uuid), FriendshipRequests.requestId == reqId).one_or_none()
        
        if requests == None:
            return False
        else:
            return True
        
    @staticmethod
    def getFriendRequestInfo(db, reqId):
        request = db.query(FriendshipRequests).filter( FriendshipRequests.requestId == reqId).one_or_none()
        
        if request == None:
            return (False, False)
        else:
            return (request.requester, request.requestee)
        
        
        
        
        
    @staticmethod
    def confirmFriendRequest(db, reqId, uuidConfirmer, uuidConfirmed):
        friendship = Friends.createFriendship(uuidConfirmer, uuidConfirmed)
        friendshipRequest = db.query(FriendshipRequests).filter(FriendshipRequests.requestId == reqId).one_or_none()
        
        if friendshipRequest == None:
            return (False, "friendshipRequestNotFound")
        else:
            db.delete(friendshipRequest)
        db.add(friendship)
        return (True, "friendshipConfirmed")


    @staticmethod
    def denyFriendRequest(db, reqId):
        friendshipRequest = db.query(FriendshipRequests).filter(FriendshipRequests.requestId == reqId).one_or_none()
        if friendshipRequest == None:
            return (False, "friendshipRequestNotFound")
        else:
            db.delete(friendshipRequest)
        return (True, "friendshipDenied")
    
    
    
    @staticmethod
    def getDistanceBetween(db, uuidA, uuidB):
        user1 = db.query(UserInfo).filter(UserInfo.uuid == uuidA).one_or_none()
        if user1 == None:
            return (False, "userNotFound")
        
        user2 = db.query(UserInfo).filter(UserInfo.uuid == uuidB).one_or_none()
        if user2 == None:
            return (False, "userNotFound")
        
        
        loc = db.query(Locations).filter(Locations.uuid == uuidA).all()
        if loc == None or len(loc) < 0:
            return (False, "locationNotFound")
        loc1 = loc[0]
        
        loc = db.query(Locations).filter(Locations.uuid == uuidB).all()
        if loc == None or len(loc) < 0:
            return (False, "locationNotFound")
        loc2 = loc[0]
        
        earthRadius = 6371000;
        theta1 = math.radians(float(loc1.latitude))
        theta2 = math.radians(float(loc2.latitude))
        delta1 = math.radians(float(loc2.latitude) - float(loc1.latitude))
        delta2 = math.radians(float(loc2.longitude) - float(loc1.longitude))
        
        a = (math.sin(delta1 / 2) * math.sin(delta1 / 2)) + ((math.cos(theta1) * math.cos(theta2)) * (math.sin(delta2 / 2) * math.sin(delta2 / 2)))
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        distance = earthRadius * c
        return (True, str(distance))


    @staticmethod
    def getFriendsList(db, uuid):
        friendsA = db.query(Friends).filter(Friends.friendA == uuid).all()
        friendsB = db.query(Friends).filter(Friends.friendB == uuid).all()
        lst = []
        if friendsA == None and friendsB == None:
            return lst
        else:
            if friendsA != None:
                for x in friendsA:
                    lst = lst + [{"friendUUID"   : x.friendB,
                                  "friendshipId" : x.friendshipId}]
            
            if friendsB != None:
                for x in friendsB:
                    lst = lst + [{"friendUUID"   : x.friendA,
                                  "friendshipId" : x.friendshipId}]
            return lst      



    @staticmethod
    def getRandomUserWithinRadius(db, uuid, distanceInMeters):
        user = db.query(UserInfo).filter(UserInfo.uuid == uuid).one_or_none()
        if user == None:
            return (False, "userNotFound")
        
        locations = db.query(Locations).filter(Locations.uuid == uuid).all()
        if locations == None or len(locations) < 1:
            return (False, "locationNotFound")
        
        location = locations[0]
        
        earthRadius = 6371000;
        oldLat = float(location.latitude)
        oldLon = float(location.longitude)
        latOffset = (float(distanceInMeters) / earthRadius) * (180.0 * math.pi)
        lonOffset =  (float(distanceInMeters) / earthRadius) * (180.0 * math.pi) / (math.cos(oldLat * (math.pi / 180.0)))
        
        maxLatitude = oldLat + latOffset
        minLatitude = oldLat - latOffset
        maxLongitude = oldLon + lonOffset
        minLongitude = oldLon - lonOffset
        
        
        foundUsers = db.query(Locations).filter(and_(and_(and_(Locations.latitude <= maxLatitude, Locations.latitude >= minLatitude), and_(Locations.longitude <= maxLongitude, Locations.longitude >= minLongitude))), Locations.uuid != uuid).all()
            
            
        if foundUsers == None or len(foundUsers) == 0:
            return (False, "noUsersFound")
        
        matchedUser = foundUsers[random.randint(0, len(foundUsers) - 1)]
        
        return (True, str(matchedUser.uuid))
    

    @staticmethod
    def getUserCountWithinArea(db, uuid, distanceInMeters):
        user = db.query(UserInfo).filter(UserInfo.uuid == uuid).one_or_none()
        if user == None:
            return (False, "userNotFound")
        
        location = db.query(Locations).filter(Locations.uuid == uuid).one_or_none()
        if location == None:
            return (False, "locationNotFound")
        
        
        earthRadius = 6371000;
        oldLat = float(location.latitude)
        oldLon = float(location.longitude)
        latOffset = (float(distanceInMeters) / earthRadius) * (180.0 * math.pi)
        lonOffset =  (float(distanceInMeters) / earthRadius) * (180.0 * math.pi) / (math.cos(oldLat * (math.pi / 180.0)))
        
        maxLatitude = oldLat + latOffset
        minLatitude = oldLat - latOffset
        maxLongitude = oldLon + lonOffset
        minLongitude = oldLon - lonOffset
        
        
        foundUsers = db.query(Locations).filter(and_(and_(and_(Locations.latitude <= maxLatitude, Locations.latitude >= minLatitude), and_(Locations.longitude <= maxLongitude, Locations.longitude >= minLongitude))), Locations.uuid != uuid).all()
            
            
        if foundUsers == None or len(foundUsers) == 0:
            return (True, "1")
        
        return (True, str(len(foundUsers) + 1))


    @staticmethod
    def setStatus(db, uuid, message):
        status = db.query(UserStatuses).filter(UserStatuses.uuid == uuid).one_or_none()
        if status == None:
            newStatus = UserStatuses.createStatus(uuid, message, "Online")
            db.add(newStatus)
            return True
        
        db.query(Locations).filter(UserStatuses.uuid == uuid).update({UserStatuses.status : message, UserStatuses.alterationTime : getTimestamp()}, synchronize_session='fetch')
        return True
        
    @staticmethod
    def setState(db, uuid, state):
        status = db.query(UserStatuses).filter(UserStatuses.uuid == uuid).one_or_none()
        if status == None:
            newStatus = UserStatuses.createStatus(uuid, "~", state)
            db.add(newStatus)
            return True
        
        db.query(Locations).filter(UserStatuses.uuid == uuid).update({UserStatuses.state : state, UserStatuses.alterationTime : getTimestamp()}, synchronize_session='fetch')        
        return True

    @staticmethod
    def getChatHistory(db, uuidA, uuidB):
        messages = db.query(Messages).filter(or_(and_(Messages.srcUuid == uuidA, Messages.desUuid == uuidB), and_(Messages.desUuid == uuidA, Messages.srcUuid == uuidB))).all()        
        
        lst = []
        if messages == None:
            return lst
        else:
            for x in messages:
                lst = lst + [{"messageData" : x.messageData,
                              "messageId"   : x.messageId,
                              "srcUser"     : x.srcUuid,
                              "destUser"    : x.desUuid,
                              "timestamp"   : x.creationTime}]
            return lst















class DatabaseEnginePlugin(plugins.SimplePlugin):
    def __init__(self, bus):
        plugins.SimplePlugin.__init__(self, bus)
        self.sa_engine = None
        self.bus.subscribe("bind", self.bind)

    def start(self):
        self.sa_engine = create_engine("mysql+pymysql://username:password@localhost/localchat", echo = False, pool_recycle = 3600)
        Base.metadata.create_all(self.sa_engine)

    def stop(self):
        if self.sa_engine:
            self.sa_engine.dispose()
            self.sa_engine = None

    def bind(self, session):
        session.configure(bind = self.sa_engine)
