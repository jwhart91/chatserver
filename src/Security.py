import rsa, base64

class Crypto():
    instance = None

    def __init__(self, pubkey, privkey):
        if Crypto.instance == None:
            with open(privkey, 'rb') as privatefile:
                keydata = str(privatefile.read(), "utf-8")
            self.serverPrivateKey = rsa.PrivateKey.load_pkcs1(keydata)

            with open(pubkey, 'rb') as publicfile:
                keydata = str(publicfile.read(), "utf-8")
            self.serverPublicKey = rsa.PublicKey.load_pkcs1(keydata)

            Crypto.instance = self
        else:
            raise "Security.Crypto can not have more than one instance. Use Security.Crypto.getInst()"

    @staticmethod
    def getInst():
        if Crypto.instance == None:
            Crypto.instance = Crypto("./keys/pubkey.txt", "./keys/privkey.txt")
        return Crypto.instance

    def encrypt(self, plainText, publicKeyOfDestUser):
        return rsa.encrypt(plainText, publicKeyOfDestUser)

    def encryptPKCS1(self, plaintext, publicKeyOfDestUser):
        userPubKey = rsa.PublicKey.load_pkcs1_openssl_pem(publicKeyOfDestUser)
        cipherText = rsa.encrypt(bytes(plaintext, "utf-8"), userPubKey)
        return base64.b64encode(cipherText)


    def decrypt(self, cryptoText):
        return rsa.decrypt(cryptoText, self.serverPrivateKey)

