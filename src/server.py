import os, os.path, json

import cherrypy
from cherrypy.lib.static import serve_file
from DataSource import DataSource, DatabaseEnginePlugin
from string import Template
from time import gmtime, strftime
from datetime import datetime
from APIv1 import APIv1
from sqlalchemy import Column, create_engine
from sqlalchemy.types import String, Integer



class Root(object):
    
    def __init__(self):
        self.lol = "lol"

    def index(self):  
        profTemplate = Template(open("./static/index.html", "r").read())
        return profTemplate.safe_substitute(CURR_TIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))


    def captcha(self, capId = ""):
        # TODO: register this captcha ID as served
        return serve_file(os.path.abspath(os.getcwd()) + "/static/captcha.png", 'application/png')
    
    @cherrypy.tools.json_out()
    def uploadImg(self, uid, fileData):
        absPath = os.path.abspath(os.getcwd())
        try:
            profImg = open(absPath + "/files/" + uid, "wb")
        
            while True:
                data = fileData.file.read(8192)
                if not data:
                    break
                profImg.write(data)
            profImg.close()
        except e:
            return [{"status" : "error",
                     "reason" : "uploadError"}]
        
        return [{"status" : "success",
                 "reason" : "uploadSuccess"}]
        
        
        
    def profImg(self, uid = ""):
        absPath = os.path.abspath(os.getcwd())
        if uid == "" or not os.path.isfile(absPath + "/files/" + uid):
            return serve_file(absPath + "/files/default_profile.png", 'application/png')
        else:
            return serve_file(absPath + "/files/" + uid, 'application/png')
        
    def activate(self, uid = ""):
        res = DataSource.checkActivation(cherrypy.request.db, uid)
        if res == True:
            profTemplate = Template(open("./static/activateSuccess.html", "r").read())
            return profTemplate.safe_substitute(CURR_TIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
        else:
            profTemplate = Template(open("./static/activateFailure.html", "r").read())
            return profTemplate.safe_substitute(CURR_TIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"), REASON = res[1])
        
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()        
    def debug(self):
        try:
            comm = cherrypy.request.json
        except:
            return [{"status" : "error"}]
        
        if "request" not in comm[0]:
            return [{"status" : "error", "reason" : "badRequest"}]
        
        if comm[0]["request"] == "getAllLogins":
            return [{
                "names" : DataSource.getAllLogins(cherrypy.request.db)
            }]
            
    
        
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def api(self):
        comm = cherrypy.request.json
        API = APIv1(comm)        
#        try:
#            comm = cherrypy.request.json
#            API = APIv1(comm)
#        except:
#            print(cherrypy.request.json)
#            error = [{"status": "error", "reason": "Not JSON"}]
#            return error

        return API.evaluate()

    index.exposed = True
    api.exposed = True
    captcha.exposed = True
    activate.exposed = True
    debug.exposed = True
    profImg.exposed = True
    uploadImg.exposed = True

def malformedRequestHandler(status, message, traceback, version):
    cherrypy.response.status = 200
    cherrypy.response.headers['Content-Type'] = 'application/json'
    errorMessage = [{"status"  : "error",
                     "response": "malformedRequest",
                     "reason"  : message}]
    return json.dumps(errorMessage)

if __name__ == '__main__':
    serverConfig = {
        'server.socket_host' : '0.0.0.0',
        'server.socket_port' : 19999,
        'engine.autoreload_on': False}

    cherrypy.config.update(serverConfig) 
    DatabaseEnginePlugin(cherrypy.engine).subscribe()
    cherrypy.tools.db = DataSource()

    staticconf = {
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static' 
        },
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd()),
            'error_page.400' : malformedRequestHandler,
            'error_page.500' : malformedRequestHandler,
            'error_page.404' : malformedRequestHandler,
            'tools.db.on': True
        },
        '/s.js':{
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.abspath("./static/s.js")
        },
        '/s.css':{
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.abspath("./static/s.css")
        },
        '/jsencrypt.js':{
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.abspath("./static/jsencrypt.min.js")
        },
        '/aes-js.js':{
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.abspath("./static/aes-js.js")
        },
        '/jquery.js':{
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.abspath("./static/jquery-3.1.1.min.js")
        },
        '/sha256.js':{
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.abspath("./static/sha256.js")
        }
    }

    webapp = Root()
    cherrypy.quickstart(webapp, '/', staticconf)

