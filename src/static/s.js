function generateKeys(){
    var keySize = 1024;
    var crypt = new JSEncrypt({default_key_size: keySize});
    crypt.getKey();
    return {"private" : crypt.getPrivateKey(),
            "public"  : crypt.getPublicKey()};
};

function hashPassword(str) {
    var shaObj = new jsSHA("SHA-256", "TEXT");
	shaObj.update(str);
	var passHash = shaObj.getHash("HEX");
    var immres = [];

    while (passHash.length >= 2){
        immres.push(parseInt(passHash.substring(0, 2), 16));
        passHash = passHash.substring(2, passHash.length);
    }
    var result = new Uint8Array(immres.length);
    for(var i = 0; i < immres.length; i++){
        result[i] = immres[i];
    }

    return result;
}


function encrypt(key, plainText){
    var data = aesjs.util.convertStringToBytes(plainText);
    var aesCtr = new aesjs.ModeOfOperation.ctr(key);
    return aesCtr.encrypt(data);
}

function decrypt(key, cipherText){
    var data = b64decode(cipherText);
    var aesCtr = new aesjs.ModeOfOperation.ctr(key);
    var plainText = aesCtr.decrypt(data);
    return plainText;
}


function b64encode(u8array){
    return window.btoa(String.fromCharCode.apply(null, u8array));
}

function b64decode(str){
    return new Uint8Array(window.atob(str).split("").map(
        function(c){
            return c.charCodeAt(0);
    }));
}

function assembleString(barray){
    var s = "";
    for(var i = 0; i < barray.length; i++){
        s += String.fromCharCode(barray[i]);
    }
    return s;
}

/****
 * Information Gatherers
 ****/

function getAllLogins(){
    $.ajax({
        url: 'debug',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request" : "getAllLogins"
        }]),

        success: function(data, textStatus, jQxhr){
            $("#allLogins").html(data);
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });
}








/****
 * Click handlers
 ****/
function signupRequest(e){
    window._signupCapID = "";
    var keys = generateKeys();
    console.log(keys);
    publicKey = keys["public"];
    privateKey = keys["private"];
    encPrivateKey = "";

    var passHash = hashPassword($("#signup-pass").val());

    encPrivateKey = b64encode(encrypt(passHash, privateKey));
    $("#signup-public-key").html(publicKey.replace(/(?:\r\n|\r|\n)/g, '<br>'));
    $("#signup-private-key").html(privateKey.replace(/(?:\r\n|\r|\n)/g, '<br>'));
    $("#signup-enc-private-key").html(encPrivateKey.replace(/(?:\r\n|\r|\n)/g, '<br>'));

    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "signup",
            "email"         : $("#signup-email").val(),
            "displayName"   : $("#signup-dname").val(),
            "publicKey"     : publicKey,
            "encPrivateKey" : encPrivateKey,
        }]),

        success: function( data, textStatus, jQxhr ){
            var struct = JSON.parse(data)[0];
            $('#signup-last-result').css("color", "red");

            if(struct["status"] == "error") {
                $('#activationLink').html("Signup failed.");

            }else if(struct["response"] == "captchaChallenge"){
                $("#signup-cap-img").data("id", struct["captchaId"]);
                url = "/captcha?capId=" + struct["captchaId"];
                $('#signup-cap-img').attr("src", url);
                $('#captchaCell').css("display", "block");
                $('#signup-last-result').css("color", "black");

            }else{
                $('#activationLink').html("Unexpected data.");
            }
            $('#signup-last-result').html(JSON.stringify(struct));

        },

        error: function(jqXhr, textStatus, errorThrown){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function checkCaptcha(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "checkCaptcha",
            "email"         : $("#signup-email").val(),
            "captchaID"     : $("#signup-cap-img").data("id"),
            "capVal"        : $("#signup-captcha-answer").val(),
            "displayName"   : $("#signup-dname").val(),
            "publicKey"     : publicKey,
            "encPrivateKey" : encPrivateKey,
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#signup-last-result').css("color", "red");

            if(struct["status"] == "error") {
                $('#signup-act-link').html("Captcha check failed.");
                $('#signup-cap-img').prop("src", "/captcha?capId=" + struct["captchaId"]);

            }else if(struct["response"] == "waitingForActivation"){
                $('#signup-act-link').prop("href", "/activate?uid=" + struct["userId"]);
                $('#signup-act-cell').css("display", "block");
                $('#signup-last-result').css("color", "black");

            }else{
                $('#signup-act-link').html("Unexpected data.");
            }
            $('#signup-last-result').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function answerChallengeKey(uuid, unsolvedChallenge){
    var crypt = new JSEncrypt();
    crypt.setPrivateKey(privateKey);
    var solvedChallenge = crypt.decrypt(unsolvedChallenge);
    console.log(solvedChallenge);

    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        async: true,
        contentType: 'application/json',
        data: JSON.stringify([{
            "request"      : "answerChallengeKey",
            "uuid"         : uuid,
            "solution"     : solvedChallenge
        }]),
        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#login-last-results').css("color", "red");
            $('#login-last-results').html(JSON.stringify(struct));

            if(struct["status"] == "error"){
                $('#login-pass').css("background-color", "pink");
                $('#login-bad-pass-notice').css("display", "block");
            }
            else if(struct["response"] == "authSessionCreated"){
                authToken = struct["authToken"];
                $('#login-pass').css("background-color", "lightgreen");
                $('#login-bad-pass-notice').css("display", "none");

                $('#login-misc-auth-token').html(authToken);
                $('#login-last-results').css("color", "black");
                $("#user-profile-image").prop("src", "/profImg?uid=" + uuid);
            }

            return;
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
            return;
    }
});

}
function getChallengeKey(uuid){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        async: false,
        contentType: 'application/json',
        data: JSON.stringify([{
            "request"       : "getChallengeKey",
            "uuid"         : uuid
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#login-last-results').css("color", "red");
            $('#login-last-results').html(JSON.stringify(struct));
            if(struct["status"] == "error"){
                $('#login-misc-challenge-key').html("FAILED");
                $('#login-misc-challenge-key').css("color", "red");
            }
            else if(struct["response"] == "issueChallengeKey"){
                challengeKey = struct["challengeKey"];
                answerChallengeKey(uuid, challengeKey);

                $('#login-misc-challenge-key').html(challengeKey);
                $('#login-last-results').css("color", "black");
            }

            return;
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
            return;
        }
    });

}

function decryptPrivateKey(pass, cipherText){
    var key = hashPassword(pass);
    var plainText = decrypt(key, cipherText);
    return assembleString(plainText);
}

function getLoginKeys(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "requestKeys",
            "email"         : $("#login-email").val()
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#login-last-results').css("color", "red");

            if(struct["status"] == "error") {
                $('#signup-cap-img').prop("src", "/captcha?capId=" + struct["captchaId"]);

            }else if(struct["response"] == "keyDelivery"){
                publicKey = struct["pubKey"];
                encPrivateKey = struct["encPrivKey"];
                uuid = struct["uuid"];
                email = struct["email"];
                var pass = $("#login-pass").val();
                privateKey = decryptPrivateKey(pass, encPrivateKey);


                $('#login-public-key').html(publicKey.replace(/(?:\r\n|\r|\n)/g, '<br>'));
                $('#login-encrypted-private-key').html(encPrivateKey.replace(/(?:\r\n|\r|\n)/g, '<br>'));
                $('#login-private-key').html(privateKey);
                $('#login-misc-uuid').html(uuid);
                $('#login-misc-email').html(email);
                $('#login-last-results').css("color", "black");

                getChallengeKey(uuid);

            }else{
                $("#login-logout-results").html("Unknown data");
            }
            $('#login-last-results').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function logoutOfAllSessions(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "logoutOfAllSessions",
            "uuid"          : uuid,
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#login-last-results').css("color", "red");

            if(struct["status"] == "error") {
                $("#login-logout-results").html("Error");
            }else if(struct["response"] == "allLoginsCleared"){
                $("#login-logout-results").html("All logins cleared");
                getAllLogins();
            }else{
                $("#login-logout-results").html("Unknown data");
            }
            $('#login-last-results').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function getAllSessions(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "getAllLogins",
            "uuid"          : uuid,
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#login-last-results').css("color", "red");

            if(struct["status"] == "error") {
                $("#login-logout-results").html("Error");
            }else if(struct["response"] == "allActiveTokens"){
                $("#login-open-sessions").html(data);

            }else{
                $("#login-logout-results").html("Unknown data");
            }
            $('#login-last-results').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function logoutOfThisSession(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "logoutOfSession",
            "uuid"          : uuid,
            "authToken"     : authToken
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#login-last-results').css("color", "red");

            if(struct["status"] == "error") {
                $("#login-logout-results").html("Error");
            }else if(struct["response"] == "loggedOutOfSession"){
                $("#login-logout-results").html("logged out of session: " + authToken);
                getAllLogins();

            }else{

            }
            $('#login-last-results').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function sendMessage(e){
    var demail = $("#send-message-email").val();
    var dstuuid = $("#send-message-uuid").val();
    var mess = $("#send-message-box").val();

    function send(key, duuid){
        var crypt = new JSEncrypt();
        crypt.setPublicKey(key);
        var encMessage = crypt.encrypt(mess);
        $('#send-message-enc-data').html(encMessage);
        $("#send-our-uuid").html(uuid);
        $.ajax({
            url: 'api',
            dataType: 'text',
            type: 'post',
            async: false,
            contentType: 'application/json',

            data: JSON.stringify([{
                "request"     : "sendMessage",
                "dstUuid"     : duuid,
                "srcUuid"     : uuid,
                "encMessage"  : encMessage,
                "authToken"   : authToken
            }]),

            success: function(data, textStatus, jQxhr){
                var struct = JSON.parse(data)[0];
                $('#send-message-last-results').css("color", "red");

                if(struct["status"] == "error") {
                    console.log("ERROR");
                }else if(struct["response"] == "sentMessage"){
                    $('#send-message-last-results').css("color", "black");

                    console.log("DONE");
                }else{
                    console.log("ERROR");
                }
                $('#send-message-last-results').html(data);
            },

            error: function( jqXhr, textStatus, errorThrown ){
                console.log(errorThrown);
                console.log(textStatus);
            }
        });
    }
    function getKeys(duuid){
            $.ajax({
                url: 'api',
                dataType: 'text',
                type: 'post',
                async: false,
                contentType: 'application/json',

                data: JSON.stringify([{
                    "request"     : "requestKeys",
                    "uuid"        : duuid
                }]),

                success: function(data, textStatus, jQxhr){
                    var struct = JSON.parse(data)[0];
                    $('#send-message-last-results').css("color", "red");

                    if(struct["status"] == "error") {

                    }else if(struct["response"] == "keyDelivery"){
                        $("#send-dest-uuid").html(duuid);
                        send(struct["pubKey"], duuid);

                    }else{

                    }
                    $('#send-message-last-results').html(JSON.stringify(struct));
                },

                error: function( jqXhr, textStatus, errorThrown ){
                    console.log(errorThrown);
                    console.log(textStatus);
                }
            });
    }
    function resolveUuid(dmail){
        console.log("resolving: " + dmail);
            $.ajax({
                url: 'api',
                dataType: 'text',
                type: 'post',
                contentType: 'application/json',

                data: JSON.stringify([{
                    "request"       : "getUuid",
                    "email"         : dmail,
                }]),

                success: function(data, textStatus, jQxhr){
                    var struct = JSON.parse(data)[0];
                    $('#send-message-last-results').css("color", "red");

                    if(struct["status"] == "error") {
                    }else if(struct["response"] == "gotUserId"){
                        getKeys(struct["userId"]);

                    }else{
                    }
                    $('#send-message-last-results').html(JSON.stringify(struct));
                },

                error: function( jqXhr, textStatus, errorThrown ){
                    console.log(errorThrown);
                    console.log(textStatus);
                }
            });
    }

    console.log("email: \"" + demail + "\"")
    console.log("uuid: \"" + dstuuid + "\"")
    if(dstuuid == ""){
        resolveUuid(demail);
    }else{
        getKeys(dstuuid);
    }

    e.preventDefault();
}

function fetchMessages(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "getMessages",
            "uuid"          : uuid,
            "authToken"     : authToken
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#get-message-last-results').css("color", "red");

            if(struct["status"] == "error") {
                $('#get-message-last-results').html(JSON.stringify(struct));
            }else if(struct["response"] == "allMessages"){
                var crypt = new JSEncrypt();
                crypt.setPrivateKey(privateKey);
                var messes = "";
                var st = struct["messages"];
                for(var i = 0; i < st.length; i++){
                    var mess = crypt.decrypt(st[i]["messageData"]);
                    messes += "'messageData' : '" + mess               + "'<br>" +
                              "'messageId'   : '" + st[i]["messageId"] + "'<br>" +
                              "'srcUser'     : '" + st[i]["srcUser"]   + "'<br>" +
                              "'timestamp'   : '" + st[i]["timestamp"] + "'<br><br><br>";
                }
                $("#get-messages-all-dec").html(messes);
                $('#get-message-last-results').css("color", "black");
                $('#get-message-last-results').html("<lots of data> (not a placeholder)");
                $('#get-message-last-results').html(JSON.stringify(struct));

            }else{
                $('#get-message-last-results').html(JSON.stringify(struct));
            }
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function sendFriendRequest(e){
    var demail = $("#send-friend-request-email").val();
    var dstuuid = $("#send-friend-request-uuid").val();
    var mess = $("#send-friend-request-box").val();

    function send(key, duuid){
        var crypt = new JSEncrypt();
        crypt.setPublicKey(key);
        var encMessage = crypt.encrypt(mess);
        $.ajax({
            url: 'api',
            dataType: 'text',
            type: 'post',
            async: false,
            contentType: 'application/json',

            data: JSON.stringify([{
                "request"      : "sendFriendRequest",
                "uuid"         : uuid,
                "prospectUuid" : duuid,
                "greeting"     : encMessage,
                "authToken"    : authToken
            }]),

            success: function(data, textStatus, jQxhr){
                var struct = JSON.parse(data)[0];
                $('#send-friend-request-last-results').css("color", "red");

                if(struct["status"] == "error") {
                    $('#send-friend-request-req-id').html(struct["reason"]);
                }else if(struct["response"] == "sentFriendRequest"){
                    $('#send-friend-request-last-results').css("color", "black");
                    $('#send-friend-request-req-id').html(struct["requestId"]);
                }else{
                    $('#send-friend-request-req-id').html("Unknown error");
                }
                $('#send-friend-request-last-results').html(JSON.stringify(struct));
            },

            error: function( jqXhr, textStatus, errorThrown ){
                console.log(errorThrown);
                console.log(textStatus);
            }
        });
    }
    function getKeys(duuid){
            $.ajax({
                url: 'api',
                dataType: 'text',
                type: 'post',
                async: false,
                contentType: 'application/json',

                data: JSON.stringify([{
                    "request"     : "requestKeys",
                    "uuid"        : duuid
                }]),

                success: function(data, textStatus, jQxhr){
                    var struct = JSON.parse(data)[0];
                    $('#send-friend-request-last-results').css("color", "red");

                    if(struct["status"] == "error") {

                    }else if(struct["response"] == "keyDelivery"){
                        $("#send-dest-uuid").html(duuid);
                        send(struct["pubKey"], duuid);

                    }else{

                    }
                    $('#send-friend-request-last-results').html(JSON.stringify(struct));
                },

                error: function( jqXhr, textStatus, errorThrown ){
                    console.log(errorThrown);
                    console.log(textStatus);
                }
            });
    }
    function resolveUuid(dmail){
        console.log("resolving: " + dmail);
            $.ajax({
                url: 'api',
                dataType: 'text',
                type: 'post',
                contentType: 'application/json',

                data: JSON.stringify([{
                    "request"       : "getUuid",
                    "email"         : dmail,
                }]),

                success: function(data, textStatus, jQxhr){
                    var struct = JSON.parse(data)[0];
                    $('#send-friend-request-last-results').css("color", "red");

                    if(struct["status"] == "error") {
                    }else if(struct["response"] == "gotUserId"){
                        getKeys(struct["userId"]);

                    }else{
                    }
                    $('#send-friend-request-last-results').html(JSON.stringify(struct));
                },

                error: function( jqXhr, textStatus, errorThrown ){
                    console.log(errorThrown);
                    console.log(textStatus);
                }
            });
    }

    if(dstuuid == ""){
        resolveUuid(demail);
    }else{
        getKeys(dstuuid);
    }

    e.preventDefault();
}

function checkFriendRequests(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"       : "checkFriendRequests",
            "uuid"          : uuid,
            "authToken"     : authToken
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#check-friend-requests-last-results').css("color", "red");

            if(struct["status"] == "error") {
                $('#check-friend-requests-last-results').html(JSON.stringify(struct));
            }else if(struct["response"] == "allFriendRequests"){
                var crypt = new JSEncrypt();
                crypt.setPrivateKey(privateKey);
                var messes = "";
                var st = struct["requests"];
                for(var i = 0; i < st.length; i++){
                    var mess = crypt.decrypt(st[i]["greeting"]);
                    messes += "'requester' : '" + st[i]["requester"] + "'<br>" +
                              "'requestId' : '" + st[i]["requestId"] + "'<br>" +
                              "'presented' : '" + st[i]["presented"] + "'<br>" +
                              "'greeting'  : '" + mess               + "'<br>" +
                              "'timestamp' : '" + st[i]["timestamp"] + "'<br><br><br>";
                }
                $("#check-friend-requests-all-dec").html(messes);
                $('#check-friend-requests-last-results').html(JSON.stringify(struct));

            }else{
                $('#check-friend-requests-last-results').html(JSON.stringify(struct));
            }
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}


function changeDisplayName(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "changeDisplayName",
            "uuid"           : uuid,
            "authToken"      : authToken,
            "newDisplayName" : $("#user-profile-display-name").val()
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#user-profile-last-results').css("color", "red");

            if(struct["status"] == "error") {
                $('#user-profile-last-results').html(JSON.stringify(struct));
            }else if(struct["response"] == "displayNameUpdated"){
                $('#user-profile-last-results').html(JSON.stringify(struct));

            }else{
                $('#user-profile-last-results').html(JSON.stringify(struct));
            }
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}





function findRandomUserByArea(e){
    var distance = $("#random-user-by-distance-meters").val();

    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"   : "findRandomUserByArea",
            "uuid"      : uuid,
            "authToken" : authToken,
            "distance"  : distance,
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#random-user-by-distance-data').css("color", "black");
            $('#random-user-by-distance-data').html(JSON.stringify(struct));

            if(struct["status"] == "error") {
                $('#random-user-by-distance-data').css("color", "red");
            }
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}


function changeLocation(e){
    var lat = $("#set-latitude").val();
    var lon = $('#set-longitude').val();

    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "updateLocation",
            "uuid"           : uuid,
            "authToken"      : authToken,
            "newLatitude"    : lat,
            "newLongitude"   : lon,
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#set-location-response-data').css("color", "black");
            $('#set-location-response-data').html(JSON.stringify(struct));

            if(struct["status"] == "error") {
                $('#set-location-response-data').css("color", "red");
            }
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}


function getDistanceBetween(e){

    var otherUuid = $('#get-distance-uuid').val();

    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"   : "getDistanceFromUser",
            "ownUuid"   : uuid,
            "authToken" : authToken,
            "otherUuid" : otherUuid,
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#get-distance-data').css("color", "black");
            $('#get-distance-data').html(JSON.stringify(struct));

            if(struct["status"] == "error") {
                $('#get-distance-data').css("color", "red");
            }
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}


function answerFriendRequest(e){
    var answer = $("input[name=answer-friend-req]:checked").val();
    var reqId = $("#answer-friend-request-uuid").val();
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "answerFriendRequest",
            "uuid"           : uuid,
            "authToken"      : authToken,
            "reqId"          : reqId,
            "answer"         : answer
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#answer-friend-request-results').html(JSON.stringify(struct));
            
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}

function getFriendsList(e){
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "getFriendsList",
            "uuid"           : uuid,
            "authToken"      : authToken
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#get-friends-list-friends').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}


function getUserInfo(e){
    var destUuid = $('#get-user-info-uuid').val();
    
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "getUserInformation",
            "uuid"           : destUuid
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#get-user-info-data').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}




function userGetImage(e){
    var destUuid = $('#get-user-info-uuid').val();
    console.log("fetching img...")
    $('#user-profile-image').removeAttr("src").attr('src', "/profImg/" + uuid);

    e.preventDefault();
}




function userSetImage(e){
    console.log("setting....");
    var fd = new FormData();    
    fd.append('fileData', e.target.files[0]);
    fd.append('uid', uuid);
    
    
    function uploadFile(){
        console.log("uploading....");
        $.ajax({
            url: 'uploadImg',
            dataType: 'text',
            type: 'post',
            processData: false,
            contentType: false,
            data: fd,
            success: function(data, textStatus, jQxhr){
                $('#user-profile-image-data').html(data);
            },

            error: function( jqXhr, textStatus, errorThrown ){
                console.log(errorThrown);
                console.log(textStatus);
            }
        });
    }
    
    uploadFile();

    e.preventDefault();
}



function getOwnInfo(e){    
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "getUserInformation",
            "uuid"           : uuid
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#set-user-data-status-box').val(struct["statusMessage"]);
            $('#set-user-data-state-box').val(struct["onlineState"]);
            $('#set-user-data-state-data').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}



function changeStatus(e){
    var status = $('#set-user-data-status-box').val();
            
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "setStatusMessage",
            "uuid"           : uuid,
            "authToken"      : authToken,
            "newStatus"      : status
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#set-user-data-state-data').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}


function changeState(e){
    var state = $('#set-user-data-state-box').val();
    $.ajax({
        url: 'api',
        dataType: 'text',
        type: 'post',
        contentType: 'application/json',

        data: JSON.stringify([{
            "request"        : "setOnlineState",
            "uuid"           : uuid,
            "authToken"      : authToken,
            "newState"       : state,
        }]),

        success: function(data, textStatus, jQxhr){
            var struct = JSON.parse(data)[0];
            $('#set-user-data-state-data').html(JSON.stringify(struct));
        },

        error: function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
            console.log(textStatus);
        }
    });

    e.preventDefault();
}



function getChatHistory(e){
    var demail = $("#get-chat-history-email").val();
    var dstuuid = $("#get-chat-history-uuid").val();

    function fetch(duuid){
        $.ajax({
            url: 'api',
            dataType: 'text',
            type: 'post',
            async: false,
            contentType: 'application/json',

            data: JSON.stringify([{
                "request"   : "getChatHistory",
                "otherUuid" : duuid,
                "uuid"      : uuid,
                "authToken" : authToken
            }]),

            success: function(data, textStatus, jQxhr){
                var struct = JSON.parse(data)[0];

                $("#get-chat-history-data").html(JSON.stringify(struct));
                if(struct["status"] == "error") {
                    $('#get-chat-history-data').css("color", "red");

                }else{
                    var his = struct["history"];
//         lst = lst + [{"messageData" : x.messageData,
//          "messageId"   : x.messageId,
//          "srcUser"     : x.srcUuid,
//          "destUser"    : x.desUuid,
//          "timestamp"   : x.creationTime}]
                    var tableSrc = "<tr><td>Self -- Friend</td></tr>";
                    for(var i = 0; i < his.length; i++){
                        tableSrc = tableSrc + "<tr><td>";
                        
                        if(his[i]["srcUser"] === uuid){
                            tableSrc = tableSrc + "<div style='text-align: left'>";
                        }else{
                            tableSrc = tableSrc + "<div style='text-align: right'>";
                        }
                        tableSrc = tableSrc + his[i]["timestamp"];
                        tableSrc = tableSrc + "<br><br>";
                        tableSrc = tableSrc + his[i]["messageData"];
                        tableSrc = tableSrc + "</td></tr>";
                    }
                    $('#chat-history-table').html(tableSrc);
                }
            },

            error: function( jqXhr, textStatus, errorThrown ){
                console.log(errorThrown);
                console.log(textStatus);
            }
        });
    }
    
    
    function resolveUuid(dmail){
        console.log("resolving: " + dmail);
        $.ajax({
            url: 'api',
            dataType: 'text',
            type: 'post',
            contentType: 'application/json',

            data: JSON.stringify([{
                "request"       : "getUuid",
                "email"         : dmail,
            }]),

            success: function(data, textStatus, jQxhr){
                var struct = JSON.parse(data)[0];

                $("#get-chat-history-data").html(JSON.stringify(struct));
                if(struct["status"] == "error") {
                    $('#get-chat-history-data').css("color", "red");

                }else{
                    fetch(struct["userId"]);
                }
            },

            error: function( jqXhr, textStatus, errorThrown ){
                console.log(errorThrown);
                console.log(textStatus);
            }
        });
    }

    console.log("email: \"" + demail + "\"")
    console.log("uuid: \"" + dstuuid + "\"")
    if(dstuuid == ""){
        resolveUuid(demail);
    }else{
        fetch(dstuuid);
    }

    e.preventDefault();
}


























